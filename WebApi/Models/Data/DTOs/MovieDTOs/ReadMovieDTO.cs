﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebApi.Models.Domain;

namespace WebApi.Models.Data.DTOs.MovieDTOs
{
    public class ReadMovieDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        [MaxLength(100)]
        public string Genre { get; set; }
        [MaxLength(4)]
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(400)]
        public string Picture { get; set; }
        [MaxLength(400)]
        public string Trailer { get; set; }
        public List<int> Characters { get; set; }
    }
}
