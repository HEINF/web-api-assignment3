﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Data.DTOs.MovieDTOs
{
    public class ReportMoviesInFranchiseDTO
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string Title { get; set; }
        [MaxLength(100)]
        public int ReleaseYear { get; set; }
    }
}
