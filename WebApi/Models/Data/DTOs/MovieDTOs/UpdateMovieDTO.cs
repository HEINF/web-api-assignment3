﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebApi.Models.Domain;

namespace WebApi.Models.Data.DTOs.MovieDTOs
{
    public class UpdateMovieDTO
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string Title { get; set; }
        [MaxLength(100)]
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(400)]
        public string Picture { get; set; }
        [MaxLength(400)]
        public string Trailer { get; set; }
    }
}
