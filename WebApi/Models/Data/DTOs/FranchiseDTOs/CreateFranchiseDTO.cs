﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Data.DTOs.FranchiseDTOs
{
    public class CreateFranchiseDTO
    {
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(600)]
        public string Description { get; set; }

    }
}
