﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebApi.Models.Domain;

namespace WebApi.Models.Data.DTOs.FranchiseDTOs
{
    public class UpdateFranchiseDTO
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(600)]
        public string Description { get; set; }

    }
}
