﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Data.DTOs.FranchiseDTOs
{
    public class ReadFranchiseDTO
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(600)]
        public string Description { get; set; }

        public List<int> Movies { get; set; }
    }
}
