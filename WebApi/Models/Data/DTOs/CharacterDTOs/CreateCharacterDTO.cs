﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebApi.Models.Domain;

namespace WebApi.Models.Data.DTOs.CharacterDTOs
{
    public class CreateCharacterDTO
    {
        [MaxLength(100)]
        public string FullName { get; set; }

        [MaxLength(50)]
        public string Gender { get; set; }
    }
}
