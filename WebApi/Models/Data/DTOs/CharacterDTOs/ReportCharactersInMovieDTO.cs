﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Data.DTOs.CharacterDTOs
{
    public class ReportCharactersInMovieDTO
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string FullName { get; set; }
    }
}
