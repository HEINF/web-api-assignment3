﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Data.DTOs.CharacterDTOs
{
    public class ReportCharactersInFranchiseDTO
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string FullName { get; set; }
    }
}
