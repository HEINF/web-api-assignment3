﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebApi.Models.Domain;

namespace WebApi.Models.Data.DTOs.CharacterDTOs
{
    public class UpdateCharacterDTO
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string FullName { get; set; }
        [MaxLength(50)]
        public string Alias { get; set; }
        [MaxLength(50)]
        public string Gender { get; set; }
        [MaxLength(400)]
        public string Picture { get; set; }
    }
}
