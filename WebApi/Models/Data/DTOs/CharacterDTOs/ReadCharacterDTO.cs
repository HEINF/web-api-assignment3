﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Data.DTOs.CharacterDTOs
{
    public class ReadCharacterDTO
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string FullName { get; set; }
        [MaxLength(50)]
        public string Alias { get; set; }
        [MaxLength(50)]
        public string Gender { get; set; }
        [MaxLength(400)]
        public string Picture { get; set; }
        public List<int> Movies { get; set; }
    }
}
