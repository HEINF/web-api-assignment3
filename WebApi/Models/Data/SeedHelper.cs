﻿using System.Collections.Generic;
using WebApi.Models.Domain;

namespace WebApi.Models.Data
{
    public class SeedHelper
    {
        /// <summary>
        /// adding movies to a list
        /// </summary>
        /// <returns>list with movie data</returns>/
        public static IEnumerable<Movie> GetMovieSeeds()
        {
            IEnumerable<Movie> movieSeeds = new List<Movie>() 
            {
            new Movie()
            {
                Id = 1,
                Title = "Harry Potter and the chamber of secrets",
                Genre = "Children, Fantasy",
                ReleaseYear = 2002,
                Director = "Chris Colombus",
                Picture = "PICTURE",
                Trailer = "https://www.youtube.com/watch?v=jBltxS8HfQ4",
                FranchiseId = 1
            },
            new Movie()
            {
                Id = 2,
                Title = "Annabelle ",
                Genre = "Horror, Mystery, Thriller",
                ReleaseYear = 2014,
                Director = "John R Leonetti",
                Picture = "PICTURE",
                Trailer = "https://www.youtube.com/watch?v=LHaoIdqfwzE",
                FranchiseId = 2
            },
            new Movie()
            {
                Id = 3,
                Title = "Star Wars IV",
                Genre = "Fantasy, Adventure",
                ReleaseYear = 1977,
                Director = "George Lucas",
                Picture = "PICTURE",
                Trailer = "https://www.youtube.com/watch?v=L-_xHEv0l-w",
                FranchiseId = 3
            },
            new Movie()
            {
                Id = 4,
                Title = "Iron man",
                Genre = "Action, Adventure, Sci-fi",
                ReleaseYear = 2008,
                Director = "Jon Favreau",
                Picture = "PICTURE",
                Trailer = "https://www.youtube.com/watch?v=8ugaeA-nMTc",
                FranchiseId = 4
            },
            new Movie()
            {
                Id = 5,
                Title = "Iron Man 2",
                Genre = "Action, Adventure, Sci-fi",
                ReleaseYear = 1999,
                Director = "Jon Favreau",
                Picture = "PICTURE",
                Trailer = "https://www.youtube.com/watch?v=BoohRoVA9WQ",
                FranchiseId = 4
       
            }

            };
            return movieSeeds;
        }

        /// <summary>
        /// adding franchises to a list
        /// </summary>
        /// <returns>list with franchises data</returns>
        public static IEnumerable<Franchise> GetFranchiseSeeds()
        {
            IEnumerable<Franchise> franchiseSeeds = new List<Franchise>()
            {
            new Franchise { 
                Id = 1, Name = "Harry Potter", Description = "The big-screen incarnation of J. K. Rowling’s boy wizard has proven to be just as profitable as the book version. Since 2001, 12 movie adaptations have been released, beginning with Harry Potter and the Sorcerer’s Stone." 
            },
            new Franchise {
                Id = 2, Name = "The conjuring universe", Description = "The Conjuring Universe is an American media franchise and shared universe centered on a series of supernatural horror films." 
            },
            new Franchise { 
                Id = 3, Name = "Star Wars", Description = "Though it's been more than 40 years since the original Star Wars film hit theaters and entranced moviegoers, since Disney purchased the franchise in 2012, they've been making up for lost time with new entries in the original space opera, plus a bunch of standalone serie" 
            },
            new Franchise { 
                Id = 4, Name = "MARVEL CINEMATIC UNIVERSE", Description = "Though it seems a bit unfair, the whole of the Marvel Cinematic Universe—including The Avengers, Iron Man, Captain America, and the Guardians of the Galaxy movies—is officially a single franchise in Hollywood's eyes. Which makes it a tough one to beat, with 28 films (and counting) in the past 12 year" 
            }
        };

            return franchiseSeeds;
        }

        /// <summary>
        /// adding characters to a list
        /// </summary>
        /// <returns>a list of characters data</returns>
        public static IEnumerable<Character> GetCharacterSeeds()
        {
            IEnumerable<Character> characterSeeds = new List<Character>()
            {
            new Character { 
                Id = 1, FullName = "Tony Stark", Alias = "Iron Man", Gender = "Male", Picture = "" 
            },
            new Character { 
                Id = 2, FullName = "Mia Form", Gender = "Female", Picture = "" 
            },
            new Character { 
                Id = 3, FullName = "James Rhodes", Alias = "Rhodey", Gender = "Male", Picture = "" 
            },
            new Character {
                Id = 4, FullName = "Han Solo", Gender = "Male", Picture = "" 
            },
            new Character {
                Id = 5, FullName = "Anakin Skywalker", Alias = "Darth Vader", Gender = "Male", Picture = "" 
            },
            new Character { 
                Id = 6, FullName = "Severus Snape", Alias = "Snape", Gender = "Male", Picture = "" 
            },
            new Character {
                Id = 7, FullName = "Nicholas de Mimsy-Porpington", Alias = "Nearly headless Nick", Gender = "Malse", Picture = "" 
            }
        };
            return characterSeeds;

        }
    }
}
