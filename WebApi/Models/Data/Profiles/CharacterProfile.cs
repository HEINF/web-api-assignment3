﻿using AutoMapper;
using System.Linq;
using WebApi.Models.Data.DTOs.CharacterDTOs;
using WebApi.Models.Domain;

namespace WebApi.Models.Data.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, ReadCharacterDTO>().ForMember(cDTO => cDTO.Movies, opt => opt.MapFrom(c => c.Movies.Select(m => m.Id).ToArray()));

            CreateMap<Character, ReportCharactersInFranchiseDTO>().ReverseMap();

            CreateMap<Character, ReportCharactersInMovieDTO>().ReverseMap();

            CreateMap<CreateCharacterDTO, Character>().ReverseMap();

            CreateMap<UpdateCharacterDTO, Character>().ReverseMap();
        }
    }
}
