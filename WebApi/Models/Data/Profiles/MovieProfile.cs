﻿using AutoMapper;
using System.Linq;
using WebApi.Models.Data.DTOs.MovieDTOs;
using WebApi.Models.Domain;

namespace WebApi.Models.Data.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, ReadMovieDTO>().ForMember(mDTO => mDTO.Characters, opt => opt.MapFrom(c => c.Characters.Select(c => c.Id).ToArray()));

            CreateMap<Movie, ReportMoviesInFranchiseDTO>().ReverseMap();

            CreateMap<CreateMovieDTO, Movie>().ReverseMap();

            CreateMap<UpdateMovieDTO, Movie>().ReverseMap();
        }
    }
}
