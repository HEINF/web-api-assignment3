﻿using AutoMapper;
using System.Linq;
using WebApi.Models.Data.DTOs.FranchiseDTOs;
using WebApi.Models.Domain;

namespace WebApi.Models.Data.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, ReadFranchiseDTO>().ForMember(fDTO => fDTO.Movies, opt => opt.MapFrom(m => m.Movies.Select(m => m.Id).ToArray()));

            CreateMap<CreateFranchiseDTO, Franchise>().ReverseMap();

            CreateMap<UpdateFranchiseDTO, Franchise>().ReverseMap();
        }
    }
}
