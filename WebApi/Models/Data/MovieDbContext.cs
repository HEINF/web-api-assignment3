﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using WebApi.Models.Domain;

namespace WebApi.Models.Data
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext(DbContextOptions options) : base(options)
        {

        }

        //tables
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Character> Characters { get; set; }

        //seeding data
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().HasData(SeedHelper.GetMovieSeeds());
            modelBuilder.Entity<Franchise>().HasData(SeedHelper.GetFranchiseSeeds());
            modelBuilder.Entity<Character>().HasData(SeedHelper.GetCharacterSeeds());
            modelBuilder.Entity<Character>()
            .HasMany(p => p.Movies)
            .WithMany(m => m.Characters)
            .UsingEntity<Dictionary<string, object>>(
                "CharactersMovies",
                r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                je =>
                {
                    je.HasKey("MovieId", "CharacterId");
                    je.HasData(
                        new { MovieId = 1, CharacterId = 6 },
                        new { MovieId = 1, CharacterId = 7 },
                        new { MovieId = 2, CharacterId = 2 },
                        new { MovieId = 3, CharacterId = 4 },
                        new { MovieId = 3, CharacterId = 1 },
                        new { MovieId = 4, CharacterId = 3 },
                        new { MovieId = 5, CharacterId = 1 },
                        new { MovieId = 5, CharacterId = 3 }
                    );
                });

        }
    }
}
