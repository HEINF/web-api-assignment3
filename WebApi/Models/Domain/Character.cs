﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Domain
{
    public class Character
    {
        //key
        public int Id { get; set; }
        //fields
        [MaxLength(50)]
        public string FullName { get; set; }
        [MaxLength(50)]
        public string Alias { get; set; }
        [MaxLength(50)]
        public string Gender { get; set; }
        [MaxLength(400)]
        public string Picture { get; set; }
        //navigation property
        public ICollection<Movie> Movies { get; set; }
    }
}
