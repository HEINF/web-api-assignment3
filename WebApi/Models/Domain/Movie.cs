﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Domain
{
    public class Movie
    {
        //primary key
        public int Id { get; set; }
        //fields
        [MaxLength(100)]
        public string Title { get; set; }
        [MaxLength(100)]
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(400)]
        public string Picture { get; set; }
        [MaxLength(400)]
        public string Trailer { get; set; }
        //Navigation Property
        public ICollection<Character> Characters { get; set; }
        // Foreign Key
        public int? FranchiseId { get; set; }
        // Navigation Property
        public Franchise Franchise { get; set; }
    }
}
