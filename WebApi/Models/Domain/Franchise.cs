﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Domain
{
    public class Franchise
    {
        //key
        public int Id { get; set; }
        //fields
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(600)]
        public string Description { get; set; }
        //navigation property
        public ICollection<Movie> Movies { get; set; }
    }
}
