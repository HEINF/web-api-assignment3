﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Models.Data;
using WebApi.Models.Data.DTOs.CharacterDTOs;
using WebApi.Models.Domain;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharacterController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public CharacterController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// get all characters from database
        /// </summary>
        /// <returns>list with characters</returns>
        // GET: api/Character
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadCharacterDTO>>> GetCharacters()
        {
            return _mapper.Map<List<ReadCharacterDTO>>( await _context.Characters.Include(c => c.Movies).ToListAsync());
        }

        /// <summary>
        /// get character by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>if no character with that id not found, else character</returns>
        // GET: api/Character/5
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadCharacterDTO>> GetCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<ReadCharacterDTO>(character);
        }

        /// <summary>
        /// update character by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        /// <returns>no content</returns>
        // PUT: api/Character/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, [FromBody] UpdateCharacterDTO character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }
            var domainCharacter = _mapper.Map<Character>(character);
            _context.Entry(domainCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Post new character to database
        /// </summary>
        /// <param name="character"></param>
        /// <returns>created character</returns>
        // POST: api/Character
        [HttpPost]
        public async Task<ActionResult<ReadCharacterDTO>> PostCharacter([FromBody] CreateCharacterDTO character)
        {
            var domainCharacter = _mapper.Map<Character>(character);
            _context.Characters.Add(domainCharacter);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacter", new { id = domainCharacter.Id }, character);
        }

        /// <summary>
        /// delete character by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>not found if no character with that id, else no content</returns>
        // DELETE: api/Character/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// checks if character exist in database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool</returns>
        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}
