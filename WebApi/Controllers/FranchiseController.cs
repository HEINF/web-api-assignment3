﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Models.Data;
using WebApi.Models.Data.DTOs.CharacterDTOs;
using WebApi.Models.Data.DTOs.FranchiseDTOs;
using WebApi.Models.Data.DTOs.MovieDTOs;
using WebApi.Models.Domain;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchiseController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public FranchiseController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// gets a list of all franchises 
        /// </summary>
        /// <returns>a list with franchisedtos</returns>
        // GET: api/Franchise
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadFranchiseDTO>>> GetFranchises()
        {

            return _mapper.Map<List<ReadFranchiseDTO>>(await _context.Franchises.Include(f => f.Movies).ToListAsync());
        }

        /// <summary>
        /// get franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns> if no franchise with that id in database return not found, return franchise</returns>
        // GET: api/Franchise/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadFranchiseDTO>> GetFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<ReadFranchiseDTO>(franchise);
        }

        /// <summary>
        /// Get all movies in franchise by franchise id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>not found if no franschise with that id is found, else a list with all the movies</returns>
        // GET: api/Franchise/Movies
        [HttpGet("movies/{id}")]
        public async Task<ActionResult<IEnumerable<ReportMoviesInFranchiseDTO>>> GetFranchisesMovies(int id)
        {
            var domainFranchise = await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(i => i.Id == id);

            if(domainFranchise == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<ReportMoviesInFranchiseDTO>>(domainFranchise.Movies.ToList());
        }

        /// <summary>
        /// get all the characters in a franchise by franchise id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>a list with all the franchise-characters</returns>
        // GET: api/Franchise/Characters
        [HttpGet("characters/{id}")]
        public async Task<ActionResult<IEnumerable<ReportCharactersInFranchiseDTO>>> GetFranchisesCharacters(int id)
        {
            // Gets a list of Franchises that includes movie navigation properties and the character navigation property within the movies
            var franchiseList = await _context.Franchises.Include(f => f.Movies).ThenInclude(m => m.Characters).ToListAsync();

            // Gets the requested franchise by id from the franchise list
            var franchise = franchiseList.Find(i => i.Id == id);

            // Gets characters from the movies in the franchise and excludes duplicates
            var characters = franchise.Movies.SelectMany(m => m.Characters.ToArray()).Distinct();

            return _mapper.Map<List<ReportCharactersInFranchiseDTO>>(characters);
        }

        /// <summary>
        /// updates franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns> no content</returns>
        // PUT: api/Franchise/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, [FromBody] UpdateFranchiseDTO franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }
            var domainFranchise = _mapper.Map<Franchise>(franchise);
            _context.Entry(domainFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// post new franchise to database
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>the added franchise</returns>
        // POST: api/Franchise
        [HttpPost]
        public async Task<ActionResult<ReadFranchiseDTO>> PostFranchise([FromBody] CreateFranchiseDTO franchise)
        {
            var domainFranchise = _mapper.Map<Franchise>(franchise);
            _context.Franchises.Add(domainFranchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = domainFranchise.Id }, franchise);
        }

        /// <summary>
        /// post movies to franchise with franchise id and a list of movie id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movies"></param>
        /// <returns> not found if no franchise with that id, else no content</returns>
        // POST: api/Franchise/Movies
        [HttpPost("movies{id}")]
        public async Task<ActionResult> PostFranchiseMovies(int id, [FromBody] List<int> movies)
        {
            var domainFranchise = await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);

            if (domainFranchise == null)
            {
                return NotFound();
            }

            foreach (var movieId in movies)
            {
                var tmpMovie = await _context.Movies.FirstOrDefaultAsync(m => m.Id == movieId);
                if (tmpMovie != null)
                {
                    domainFranchise.Movies.Add(tmpMovie);
                }
            }

            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// deletes franchise from database by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns> not found if no franchise with that id, else no content</returns>
        // DELETE: api/Franchise/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// checks if franchise exist in database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool</returns>
        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
