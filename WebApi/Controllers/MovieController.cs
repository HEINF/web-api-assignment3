﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Models.Data;
using WebApi.Models.Data.DTOs.CharacterDTOs;
using WebApi.Models.Data.DTOs.MovieDTOs;
using WebApi.Models.Domain;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MovieController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public MovieController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets a list of all the movies
        /// </summary>
        /// <returns>a list of movies</returns>
        // GET: api/Movie
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadMovieDTO>>> GetMovies()
        {
            return _mapper.Map<List<ReadMovieDTO>>(await _context.Movies.Include(c => c.Characters).ToListAsync());
        }

        /// <summary>
        /// Gets movie by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>a movie</returns>
        // GET: api/Movie/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadMovieDTO>> GetMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<ReadMovieDTO>(movie);
        }

        /// <summary>
        /// get all the characters in a movie by movie id
        /// </summary>
        /// <param name="id"></param>
        /// <returns> a list of characters </returns>
        // GET: api/Movie/5
        [HttpGet("characters/{id}")]
        public async Task<ActionResult<IEnumerable<ReportCharactersInMovieDTO>>> GetMovieCharacters(int id)
        {
            var domainMovie = await _context.Movies.Include( m => m.Characters).FirstOrDefaultAsync(m => m.Id == id);

            if (domainMovie == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<ReportCharactersInMovieDTO>>(domainMovie.Characters.ToList());
        }

        /// <summary>
        /// Updates movie by id
        /// throws DbUpdateConcurrencyException if changes cant be saved
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns>bad request if no movie with that id exist, no content returned </returns>
        // PUT: api/Movie/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, [FromBody] UpdateMovieDTO movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }

            var domainMovie = _mapper.Map<Movie>(movie);
            _context.Entry(domainMovie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// post characters to movie by movie id
        /// characters is list of int with characters id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characters"></param>
        /// <returns>if movie isnt in database return not found, return no content</returns>
        // PUT: api/Movie/5
        [HttpPost("characters/{id}")]
        public async Task<IActionResult> PutCharacterMovie(int id, [FromBody] List<int> characters)
        {
            //get movie with characters
            var domainMovie = await _context.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == id);

            if (domainMovie == null)
            {
                return NotFound();
            }
            //loops the list of int with charactes id
            foreach(var characterId in characters)
            {
                var tempCharacter = await _context.Characters.FirstOrDefaultAsync(c => c.Id == characterId);
                if (tempCharacter != null)
                {
                    domainMovie.Characters.Add(tempCharacter);
                }
            }

                await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// post movie to database
        /// </summary>
        /// <param name="movie"></param>
        /// <returns> returns readmoviedto</returns>
        // POST: api/Movie
        [HttpPost]
        public async Task<ActionResult<ReadMovieDTO>> PostMovie([FromBody] CreateMovieDTO movie)
        {
            var domainMovie = _mapper.Map<Movie>(movie);
            _context.Movies.Add(domainMovie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = domainMovie.Id }, movie);
        }

        /// <summary>
        /// Deletes movie in database by movie id
        /// </summary>
        /// <param name="id"></param>
        /// <returns> if no movie it returns not found, return no contetn</returns>
        // DELETE: api/Movie/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// checks if movie exist in database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool</returns>
        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}
