﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class InitalDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Alias = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(600)", maxLength: 600, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Genre = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    ReleaseYear = table.Column<int>(type: "int", maxLength: 4, nullable: false),
                    Director = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: true),
                    Trailer = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CharactersMovies",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false),
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharactersMovies", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_CharactersMovies_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharactersMovies_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, "Iron Man", "Tony Stark", "Male", "" },
                    { 2, null, "Mia Form", "Female", "" },
                    { 3, "Rhodey", "James Rhodes", "Male", "" },
                    { 4, null, "Han Solo", "Male", "" },
                    { 5, "Darth Vader", "Anakin Skywalker", "Male", "" },
                    { 6, "Snape", "Severus Snape", "Male", "" },
                    { 7, "Nearly headless Nick", "Nicholas de Mimsy-Porpington", "Malse", "" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "The big-screen incarnation of J. K. Rowling’s boy wizard has proven to be just as profitable as the book version. Since 2001, 12 movie adaptations have been released, beginning with Harry Potter and the Sorcerer’s Stone.", "Harry Potter" },
                    { 2, "The Conjuring Universe is an American media franchise and shared universe centered on a series of supernatural horror films.", "The conjuring universe" },
                    { 3, "Though it's been more than 40 years since the original Star Wars film hit theaters and entranced moviegoers, since Disney purchased the franchise in 2012, they've been making up for lost time with new entries in the original space opera, plus a bunch of standalone serie", "Star Wars" },
                    { 4, "Though it seems a bit unfair, the whole of the Marvel Cinematic Universe—including The Avengers, Iron Man, Captain America, and the Guardians of the Galaxy movies—is officially a single franchise in Hollywood's eyes. Which makes it a tough one to beat, with 28 films (and counting) in the past 12 year", "MARVEL CINEMATIC UNIVERSE" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 1, "Chris Colombus", 1, "Children, Fantasy", "PICTURE", 2002, "Harry Potter and the chamber of secrets", "https://www.youtube.com/watch?v=jBltxS8HfQ4" },
                    { 2, "John R Leonetti", 2, "Horror, Mystery, Thriller", "PICTURE", 2014, "Annabelle ", "https://www.youtube.com/watch?v=LHaoIdqfwzE" },
                    { 3, "George Lucas", 3, "Fantasy, Adventure", "PICTURE", 1977, "Star Wars IV", "https://www.youtube.com/watch?v=L-_xHEv0l-w" },
                    { 4, "Jon Favreau", 4, "Action, Adventure, Sci-fi", "PICTURE", 2008, "Iron man", "https://www.youtube.com/watch?v=8ugaeA-nMTc" },
                    { 5, "Jon Favreau", 4, "Action, Adventure, Sci-fi", "PICTURE", 1999, "Iron Man 2", "https://www.youtube.com/watch?v=BoohRoVA9WQ" }
                });

            migrationBuilder.InsertData(
                table: "CharactersMovies",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 6, 1 },
                    { 7, 1 },
                    { 2, 2 },
                    { 4, 3 },
                    { 1, 3 },
                    { 3, 4 },
                    { 1, 5 },
                    { 3, 5 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharactersMovies_CharacterId",
                table: "CharactersMovies",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharactersMovies");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
