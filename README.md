# Movie Character API
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Install
Clone project into Visual Studio.
Requires a instance of SQL server to be accessible with read and write privileges.
Change the default connectionstring in appsettings.json to point to the SQL server instance.

## Usage
Build and run in Visual Studio. This wil open your default browser and the API can then be tested through the use of Swagger.

## Maintainers

Thea Gunnarsson and Filip Hein.

## Contributing

Not open for contributions.

## License

Unknown
